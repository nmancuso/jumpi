#!/usr/bin/env python3
import os
import os.path
import subprocess as s
import sys
import argparse
import sqlite3
import pandas as pd

from sqlite3 import Error

## Database connection

conn = sqlite3.connect('jumpi.db')
c = conn.cursor()

## Get some systems details
CUR_USER = os.getlogin()
PLATFORM = sys.platform 

if PLATFORM == "darwin":
    PRIV_SSH_DIR = "/Users/%s/.ssh" % (CUR_USER)
elif PLATFORM == "linux":
    PRIV_SSH_DIR = "/home/%s/.ssh" % (CUR_USER)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

## Database functions



def init_db():
    database = r"./jumpi.db"
    conn = create_connection(database)
    if (conn):
        print('Connected to Database.')
    else:
        print('Failed to connect to the database.')
    return conn



def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def create_connection(db_file):
    sql_create_connections_table = """ CREATE TABLE `connections` (
	`id` INTEGER PRIMARY KEY AUTOINCREMENT,
	`name` VARCHAR(80),
	`hostname` VARCHAR(255),
	`username` VARCHAR(255),
	`port` INT(5) DEFAULT '22'
    ); """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def get_list():
    dataframe = pd.read_sql("""SELECT * FROM connections""", conn)
    print(dataframe.to_string(index=False))

def get_connection(name):
    c.execute("SELECT name, hostname, username, port FROM connections where id =:name", {'name': name}) 

    cd = c.fetchone()
    conn.commit()
    command = "ssh -p " + str(cd[3]) + " " + cd[2] + "@" + cd[1]
    print(command)
    
    os.system(command)
    conn.close()

def del_connections(id):
    sql = '''DELETE FROM connections WHERE ID=?'''
    cur = conn.cursor()
    cur.execute(sql, (id,))
    conn.commit()
    conn.close()
    return cur.rowcount

def create_conn(conn, task):
    sql = '''INSERT INTO connections (name,hostname,username,port)
              VALUES(?,?,?,?)'''
    cur = conn.cursor()
    cur.execute(sql, task)
    conn.commit()
    return cur.lastrowid

def key_present():
    if "id_rsa" in os.listdir(PRIV_SSH_DIR):
        return True
    else:
        return False

def gen_key():
    os.chdir(PRIV_SSH_DIR)
    if key_present():
        print("A key is already present.")
    else:
        # Genarate private key
        s.call('ssh-keygen', shell=True)

def push_key(user, host, ssh_port=22):
    os.chdir(PRIV_SSH_DIR)
    if key_present():
        if PLATFORM != "linux":
            if "ssh-copy-id" in os.listdir("/usr/local/bin"):
                print("SSH key found. Pushing key to remote server")
                command = "ssh-copy-id -p %s %s@%s" % (ssh_port, user, host)
                s.call(command, shell=True)
            else:
                print(
                    "ssh-copy-id required for Mac Users. Use --help for more information.")
    else:
        print("A SSH key is required. Run script again with action set as GenKey")



def read_edit():
    conn = init_db()

def read_input():
    conn = init_db()
    print('=' * 88)
    print(f"{bcolors.HEADER}JUMPI SSH CONFIG{bcolors.ENDC}")
    print(f"{bcolors.OKGREEN}This section lets you create, edit or delete records. Select which you'd like to do.{bcolors.ENDC}")
    print('You are currently logged in as',CUR_USER)
    print('=' * 88)
    print(f"{bcolors.BOLD}CONFIG{bcolors.ENDC}")
    print(f"{bcolors.BOLD}={bcolors.ENDC}" * 88 )
    print('1 - List all')
    print('2 - Create New')
    print('3 - Edit')
    print('4 - Delete')
    print('5 - Select keys')
    print('6 - Add New keys')
    print('7 - Delete keys')
    print('=' * 88)


    while True:
        # main
        option = str(input('What would you like to do?: '))
        
        if option == "1":
            get_list()
        elif option == "2":
            read_input_new()
            break
        elif option == "3":
            read_input_new()
            break
        elif option == "4":
            del_selection = int(input("Enter the ID of the connection you'd like to delete?: "))
            try:
                with conn:
                    results_del = del_connections(conn, del_selection)
                    print(f"{bcolors.OKGREEN} {results_del} row(s) deleted.{bcolors.ENDC}")
                break
            except Error as e:
                print(e)
            finally:
                if conn:
                    conn.close()
            break
        elif option == "5":
            print(f"{bcolors.FAIL}This option is still under development and not currently available.{bcolors.ENDC}")
            break
        elif option == "6":
            print(f"{bcolors.FAIL}This option is still under development and not currently available.{bcolors.ENDC}")
            break
        elif option == "7":
            print(f"{bcolors.FAIL}This option is still under development and not currently available.{bcolors.ENDC}")
            break

 

def read_input_new():
    conn = init_db()
    print('=' * 88)
    print('JUMPI SSH SCRIPT')
    print('This script will send over the local ssh public keys to the host below.')
    print('You are currently logged in as',CUR_USER)
    print('=' * 88)
    while True:
        # main
        name = str(input('What Friendly name should I use?: '))
        hostname = str(input('IP/Hostname: '))
        username = str(input("What username should I use to login?: "))
        while True:
            try:
                    ssh_port = int(input("What Port? (default is 22): ") or "22")
                    break
            except ValueError:
                 print("Please enter a number.")
                 continue
            break
        break

    print('='* 88)
    print('Perfect! Please confirm the information below.')
    print('='* 88)
    print('Friendly Name:',name)
    print('Hostname/IP:',hostname)
    print('Username:',username)
    print('Port:',ssh_port)

    confirmed = str(input('Are you satisfied with the above? (Y/n): ') or "Y")
    if confirmed in ('y', 'Y'):
        push_key(username, hostname, ssh_port)
    else:
        read_input_new()

    save_session = str(input('Would you like to save this session for future use?: (Y/n):') or "Y")
    if save_session in ('y', 'Y'):
        sql_save_session = (name, hostname, username, ssh_port)
        try:
            with conn:
                print(name, hostname, username, ssh_port)
                create_conn(conn, sql_save_session)
                
        except Error as e:
            print(e)
        finally:
            if conn:
                conn.close()
# def new(name):
#     results = sqlquery



def main():
# Start Script
    parser = argparse.ArgumentParser(
        description="Uses the ssh-keygen and ssh-copy-id commands found on Mac and Linux systems. Mac Users will need to install ssh-copy-id before attempting to use this script. If you do not have Homebrew installed, please visit https://github.com/beautifulcode/ssh-copy-id-for-OSX for the install. If you do have Homebrew installed, run the command brew install ssh-copy-id in Terminal.")
    parser.add_argument("action", choices=[
                        "GenKey", "PushKey", "Config", "config", "List", "list", "Connect", 'connect'], help="Action to be preformed")
    parser.add_argument("-u", "--user", help="SSH username")
    parser.add_argument("-s", "--host", help="IP or FQDN of server")
    parser.add_argument("-p", "--port", help="SSH port number")
    parser.add_argument("-n", "--name", help="Friendly name")
    parser.add_argument("-rm", "--remove", help="Delete and instance by ID")
    args = parser.parse_args()
    if (args.action == "GenKey"):
        gen_key()
    elif (args.action == "PushKey"):
        if args.user and args.host:
            if args.port:
                push_key(args.user, args.host, args.port)
            else:
                push_key(args.user, args.host)
        else:
            print("-u and -s are required for action PushKey. Use -h for Help.")
    elif (args.action == "Config" or args.action == "config"):
        read_input()
    elif (args.action == "Connect" or args.action == "connect"):
        get_connection(args.name)
    elif (args.action == "List" or args.action == "list"):
        get_list()

if __name__ == "__main__":
    main()
